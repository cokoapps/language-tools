# language tools

Language Tools for Wax editor

## Instructions

Provide the following `ENV` variables:

```
export LANGUAGE_PORT (local machine's port where language tools server will be exposed)
```

Execute `docker-compose up`
